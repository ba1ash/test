# README

``` shell
docker-compose build
docker-compose run web rspec
```

- An obvious point for improvement is to add a pagination to list endpoints. It should help in controlling the response time during the application data growth.
- It would be nice to add a dedicated "presentation" layer instead of responding with model.to_json or model.slice.to_json.
