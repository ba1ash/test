class CreateNotifications < ActiveRecord::Migration[7.0]
  def change
    create_table :notifications do |t|
      t.string :title, null: false
      t.text :description, null: false
      t.datetime :desired_delivery_date, null: false
      t.string :user_ids, array: true

      t.timestamps
    end
  end
end
