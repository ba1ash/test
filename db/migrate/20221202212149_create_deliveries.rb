class CreateDeliveries < ActiveRecord::Migration[7.0]
  def change
    create_table :deliveries do |t|
      t.references :user, null: false, foreign_key: true
      t.references :notification, null: false, foreign_key: true
      t.datetime :delivered_at

      t.timestamps
    end
    add_index :deliveries, [:user_id, :notification_id], unique: true
  end
end
