class CreateUsers < ActiveRecord::Migration[7.0]
  def up
    execute 'CREATE EXTENSION "uuid-ossp"'
    create_table :users do |t|
      t.boolean :admin, null: false
      t.uuid :api_key, null: false, default: 'uuid_generate_v4()'

      t.timestamps
    end
    add_index :users, :api_key, unique: true
  end

  def down
    remove_index :users, :api_key
    drop_table :users
    execute 'DROP EXTENSION "uuid-ossp"'
  end
end
