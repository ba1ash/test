db = Rails.env.development? ? 0 : 1

Sidekiq.configure_server do |config|
  config.redis = { url: "redis://redis:6379/#{db}" }
end

Sidekiq.configure_client do |config|
  config.redis = { url: "redis://redis:6379/#{db}" }
end
