class Root < Grape::API
  format :json

  helpers do
    def current_user
      api_key = headers['Api-Key']
      @current_user ||= User.where(api_key: api_key).first
    end

    def authorize_client!
      error!('401 Unauthorized', 401) unless current_user
    end

    def authorize_admin!
      error!('401 Unauthorized', 401) unless current_user&.admin?
    end
  end

  namespace :client do
    get :notifications do
      authorize_client!
      deliveries = Delivery
                     .select("deliveries.id, notifications.title, notifications.description")
                     .joins(:notification)
                     .where(user_id: current_user.id, delivered_at: nil)
      MarkAsDeliveredJob.perform_async(deliveries.map(&:id))
      deliveries.map { |d| d.slice(:title, :description) }
    end
  end

  namespace :admin do
    before { authorize_admin! }

    resource :notifications do
      get do
        Notification.all
      end

      route_param :id do
        get '/deliveries' do
          Delivery.where(notification_id: params[:id])
        end
      end

      params do
        requires :title, type: String
        requires :description, type: String
        requires :user_ids, type: Array
        optional :desired_delivery_date, type: DateTime, values: -> (v) { v >= DateTime.current + 1.minute }
      end
      post do
        n = Notification.create!(title: params[:title],
                                 description: params[:description],
                                 desired_delivery_date: params[:desired_delivery_date],
                                 user_ids: params[:user_ids])
        NotificationJob.perform_async(n.id)
        n
      end
    end
  end
end
