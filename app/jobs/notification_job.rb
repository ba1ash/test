class NotificationJob
  include Sidekiq::Job
  sidekiq_options retry: 5

  def perform(notification_id)
    n = Notification.find(notification_id)
    insert_attrs = n.user_ids.map do |user_id|
      {
        user_id: user_id,
        notification_id: n.id
      }
    end
    delivery_ids = Delivery.insert_all(insert_attrs, returning: [:id]).rows.flatten
    delivery_ids.each { |id| DeliveryJob.perform_at(n.desired_delivery_date, id) }
  end
end
