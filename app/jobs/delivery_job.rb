class DeliveryJob
  include Sidekiq::Job
  sidekiq_options retry: 5

  def perform(delivery_id)
    ActiveRecord::Base.transaction do
      d = Delivery.lock.where(id: delivery_id, delivered_at: nil).first!
      n = d.notification
      u = d.user
      MockPushService.send(title: n.title, description: n.description, token: u.api_key)
      d.delivered_at = DateTime.current
      d.save!
    end
  end
end
