class MarkAsDeliveredJob
  include Sidekiq::Job

  def perform(delivery_ids, delivered_at)
    Delivery
      .where(id: delivery_ids, delivered_at: nil)
      .update_all(delivered_at: delivered_at)
  end
end
