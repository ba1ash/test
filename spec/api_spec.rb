require 'rails_helper'

RSpec.describe '', type: :request do
  let(:user) do
    create(:user, admin: admin).reload
  end
  let(:admin) { false }

  describe 'GET /client/notifications' do
    context 'when there is no Api-Key' do
      it do
        get '/client/notifications'

        expect(response.code).to eq('401')
        expect(JSON.parse(response.body)).to eq({"error" => "401 Unauthorized"})
      end
    end

    context 'when there is a client Api-Key' do
      it do
        extra_user = create(:user)
        n1 = create(:notification, title: '1')
        n2 = create(:notification, title: '2')
        delivery1 = create(:delivery, notification: n1, user: user)
        delivery2 = create(:delivery, notification: n2, user: extra_user)

        get '/client/notifications', headers: { 'Api-Key' => user.api_key }

        expect(response.code).to eq('200')
        expect(JSON.parse(response.body)).to eq(
          [{"description"=>"Description", "title"=>"1"}]
        )
        queue = Sidekiq::Queue.new
        expect(queue.size).to eq(1)
        parsed_job = JSON.parse(queue.first.value)
        expect(parsed_job['args']).to eq([[delivery1.id]])
        expect(parsed_job['class']).to eq('MarkAsDeliveredJob')
      end
    end
  end

  describe 'GET /admin/notifications' do
    context 'when there is no Api-Key' do
      it do
        get '/admin/notifications'
        expect(response.code).to eq('401')
        expect(JSON.parse(response.body)).to eq({"error" => "401 Unauthorized"})
      end
    end

    context 'when there is a client Api-Key' do
      it do
        get '/admin/notifications', headers: { 'Api-Key' => user.api_key }
        expect(response.code).to eq('401')
        expect(JSON.parse(response.body)).to eq({"error" => "401 Unauthorized"})
      end
    end

    context 'when there is an admin Api-Key' do
      let(:admin) { true }

      it do
        create(:notification)
        create(:notification)

        get '/admin/notifications', headers: { 'Api-Key' => user.api_key }

        expect(response.code).to eq('200')
        r = JSON.parse(response.body)
        expect(r.count).to eq(2)
      end
    end
  end

  describe 'GET /admin/notifications/:id/deliveries' do
    context 'when there is no Api-Key' do
      it do
        get '/admin/notifications/1/deliveries'

        expect(response.code).to eq('401')
        expect(JSON.parse(response.body)).to eq({"error" => "401 Unauthorized"})
      end
    end

    context 'when there is a client Api-Key' do
      it do
        get '/admin/notifications/1/deliveries', headers: { 'Api-Key' => user.api_key }

        expect(response.code).to eq('401')
        expect(JSON.parse(response.body)).to eq({"error" => "401 Unauthorized"})
      end
    end

    context 'when there is an admin Api-Key' do
      let(:admin) { true }

      it do
        extra_user = create(:user)
        n1 = create(:notification, title: '1')
        n2 = create(:notification, title: '2')
        delivery1 = create(:delivery, notification: n1, user: user)
        delivery2 = create(:delivery, notification: n1, user: extra_user)
        delivery3 = create(:delivery, notification: n2, user: extra_user)

        get "/admin/notifications/#{n1.id}/deliveries", headers: { 'Api-Key' => user.api_key }

        expect(response.code).to eq('200')
        r = JSON.parse(response.body)
        expect(r.count).to eq(2)
      end
    end
  end

  describe 'POST /admin/notifications' do
    context 'when there is no Api-Key' do
      it do
        get '/admin/notifications/1/deliveries'

        expect(response.code).to eq('401')
        expect(JSON.parse(response.body)).to eq({"error" => "401 Unauthorized"})
      end
    end

    context 'when there is a client Api-Key' do
      it do
        get '/admin/notifications/1/deliveries', headers: { 'Api-Key' => user.api_key }

        expect(response.code).to eq('401')
        expect(JSON.parse(response.body)).to eq({"error" => "401 Unauthorized"})
      end
    end

    context 'when there is an admin Api-Key' do
      let(:admin) { true }

      it do
        body = {
          title: 'Title',
          description: 'Description',
          user_ids: [1,2,3,4],
          desired_delivery_date: DateTime.now + 1.day
        }.to_json

        post '/admin/notifications', params: body, headers: {"CONTENT_TYPE" => "application/json", 'Api-Key' => user.api_key}

        expect(response.code).to eq("201")
        expect(Notification.count).to eq(1)
        queue = Sidekiq::Queue.new
        expect(queue.size).to eq(1)
        parsed_job = JSON.parse(queue.first.value)
        expect(parsed_job['args']).to eq([Notification.first.id])
        expect(parsed_job['class']).to eq('NotificationJob')
      end
    end
  end
end
