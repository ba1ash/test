FactoryBot.define do
  factory :user do
    admin { false }
  end

  factory :notification do
    title { 'Title' }
    description { 'Description' }
    user_ids { [1, 2, 3, 4] }
    desired_delivery_date { DateTime.current + 1.hour }
  end

  factory :delivery do
    notification
    user
  end
end
