require 'rails_helper'

describe DeliveryJob do
  describe '#perform' do
    it do
      user = create(:user)
      n = create(:notification, user_ids: [user.id])
      d = create(:delivery, user: user, notification: n)
      allow(MockPushService).to receive(:send)
      expect(Delivery.first.delivered_at).to be_nil

      described_class.new.perform(d)

      expect(Delivery.first.delivered_at).to be_present
    end
  end
end

describe MarkAsDeliveredJob do
  describe '#perform' do
    it do
      user1 = create(:user)
      user2 = create(:user)
      n = create(:notification, user_ids: [user1.id, user2.id])
      delivery1 = create(:delivery, user: user1, notification: n)
      delivery2 = create(:delivery, user: user2, notification: n)
      delivered_at = DateTime.current

      described_class.new.perform([delivery1, delivery2], delivered_at)

      expect(delivery1.reload.delivered_at).to be_present
      expect(delivery2.reload.delivered_at).to be_present
    end
  end
end

describe NotificationJob do
  describe '#perform' do
    it do
      user1 = create(:user)
      user2 = create(:user)
      n = create(:notification, user_ids: [user1.id, user2.id])

      described_class.new.perform(n.id)

      expect(Delivery.count).to eq(2)
      queue = Sidekiq::ScheduledSet.new
      expect(queue.size).to eq(2)
      expect(JSON.parse(queue.first.value)['class']).to eq("DeliveryJob")
    end
  end
end
